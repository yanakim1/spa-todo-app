import React from "react";
import "./Footer.css";

export default function Footer() {
  return (

    <footer>
      <div className="footer">
        <div className="container footer__container">
          <div className="footer__info">
            <p className="copyright-bold">© ToDo App, 2022.</p>
            <p className="footer__callback">Для связи с разработчиком звоните <a className="link" href="tel:+79112990590">+7 911 299 05 90</a>. <br />
            Предложения по сотрудничеству отправляйте на почту <a className="link" href="mailto:ykim.89@mail.ru">ykim.89@mail.ru</a></p>
           <p><a className="link" href="#top">Наверх</a></p>
          </div>

        

          
        </div>
      </div>
    </footer>

  )
}