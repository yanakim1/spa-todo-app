import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";

export default function Header() {
  return (

    <header className="header-container">
      <div className="container header__container">
        <div className="header">
          <div>
            <img src="./img/task-list-svgrepo-com.svg" alt="лого" height="35" />
          </div>
          <div className="header_style">
            <Link to='/' className="header_link" style={{color: "#5E81FE"}}><span style={{color: "#757E9A"}}>To</span>Do List</Link>
          </div>
        </div>

       
      </div>
    </header>

  )
}
