import React from "react";
import {Link} from "react-router-dom";
import "./Project.css";

export default function Project(props) {

  const { projectName, projectAbout } = props;

  return (
    <div className="project__choise">
      <div className="project__info">
        <h2>{projectName}</h2>
        <div className="project__text">
        <p>{projectAbout}</p>
        <Link className="linkBtn" to="/tasklist">К проекту</Link>
        </div>
      </div>
    </div>
  )
}