import React from "react";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import Project from '../../components/Project/Project';

import "./HomePage.css";

export default function HomePage() {

  return (
    <div className="body">
      <Header />

      <div className="container">

        <div className="main__container">
          <span className="main__project-choice">Выберите проект</span>

          <Project
            projectName={"Проект 1"}
            projectAbout={`ToDo App`}
          />
          <Project
            projectName={"Проект 2"}
            projectAbout={`Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
            nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`}
          />

        </div>

      </div>
      <Footer />
    </div>

  )

}